# About me

![Jorge Luis Apaza Portilla]

Hello, my name is Jorge Luis Apaza Portilla, I have a degree in Industrial Relations, in recent years I have worked as a teacher at the IESTP JORGE BASADRE GROHMAN, developing young people in technical skills that the current market requires, in the same way I have seen the need to the region in terms of employability of resources for the region's products, the opportunity offered by the FabLab is an ambitious opportunity that promises a future for the region as well as the opportunity for it to be integrated into the international technological vanguard.

## My background

I was born in Arequipa city

### Previous work

Previously, I worked as coordinator of Talent Management at the UAP, Tambopata subsidiary.
I worked as an industry analyst, looking at integration processes, costs, personnel in mass consumption.

### Projet GUIDANCE SENSORS

Elaboration of a small tractor that has sensors that allow to measure the humidity, state of the land and species that can be found in the territory, this will allow an adequate sowing and use of hydrological resources to be elaborated, although we are in a geographical area with enough rain, and a land enriched in minerals, we have to start using resources and spaces properly, this will allow us to avoid floods, crop losses, pest growth, respect spaces for animals necessary to maintain the balance of nature , in the same way it will allow a quality harvest.

